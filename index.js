const http = require("http");
const express = require("express");
const app = express();

const hostname = "localhost";
const port = 3000;

//
app.get("/api/v1/test/:number", (req, res) => {
  const { number } = req.params;
  const sequence = [];
  let total = 0;

  if (!isNaN(number) && number <= 100 && number > 0) {
    // define first 2 index
    if (number >= 1) {
      sequence.push(0);
    }
    if (number >= 2) {
      sequence.push(1);
    }

    //sequence method
    for (let i = 2; i < number; i++) {
      const nextsequence = sequence[i - 1] + sequence[i - 2];
      sequence.push(nextsequence);
    }

    //sequence calculate
    for (let i = 0; i < sequence.length; i++) {
      total = total + sequence[i];
    }

    //response
    res.json({
      "member-count": Number(number),
      sequence: sequence,
      total: Number(total),
    });
  } else {
    if (number > 100 || number <= 0) {
      // out of range
      res.status(400).json({
        error: "Invalid parameter. Please enter number between 1-100.",
      });
    } else {
      // input not number
      res
        .status(400)
        .json({ error: "Invalid parameter. Please enter number only." });
    }
  }
});

app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/api/v1/test/1`);
});
